<?php

/**
 * @file
 * Drush functions for label scanner.
 */

/**
 * Implements hook_drush_command().
 */
function label_scanner_drush_command() {

  $items['label-scan'] = array(
    'description' => dt('Scan all labels and list invalid ones.'),
    'aliases' => array('lbsc'),
    'callback' => 'label_scanner_scan_labels',
  );

  $items['email-scan'] = array(
    'description' => dt('Scan for invalid email domains.'),
    'aliases' => array('emsc'),
    'callback' => 'label_scanner_scan_emails',
  );

  return $items;
}

/**
 * Callback function for hook_drush_command().
 */
function label_scanner_scan_labels() {
  // Load label scanner settings.
  $data_mapper = new LabelScannerDataMapper();
  $scanner = new LabelScanner($data_mapper->getValidator());
  $scanner->attach(new DrushNotification());

  // Load and scan components.
  $components = new ComponentIterator();
  $components->setLoader(new WebformComponentsLoader($data_mapper));
  foreach ($components as $one_component) {
    $scanner->isValid($one_component);
  }

  $scanner->notify("Label");
}

function label_scanner_scan_emails() {
  // Load label scanner settings.
  $data_mapper = new LabelScannerDataMapper();
  $scanner = new LabelScanner($data_mapper->getDomainValidator());
  $scanner->attach(new DrushNotification());

  // Load and scan components.
  $components = new WebformEmailIterator();
  $components->setLoader(new WebformEmailsLoader($data_mapper));
  foreach ($components as $one_component) {
    $scanner->isValidDomain($one_component);
  }

  $scanner->notify("Email");
}
/**
 * Implements hook_drush_help().
 */
function label_scanner_drush_help($section) {
  switch ($section) {
    case 'drush:label-scan':
      return dt('Scan all components to validate their labels. Label is invalid if it contains one of the restricted words.');
    case 'drush:email-scan':
      return dt('Scan emails to validate their domain name.');
  }

}
