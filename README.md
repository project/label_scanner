LABEL SCANNER MODULE
=====================


INTRODUCTION
------------

Label scanner module scans webform labels for specific words (referred to as
*restricted words*) and sends/displays notification(s) if a webform label
contains a restricted word.

This module was implemented to verify that the information collected through
webforms respects confidential information policies. We wanted to scan existing
webforms and be notified of newly created webforms which collect confidential
data such as credit card number, citizenship information or birth date...
But this module can be used for other use cases.

This module is intended to be easily extensible. It can be extended by adding
other types of components to scan or other notification methods.

* For a full description of the module, visit the sandbox project page:
   https://www.drupal.org/sandbox/karolinam/2934749

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2934749

REQUIREMENTS
-------------

None.

INSTALLATION
-------------

* Install as usual, see http://drupal.org/node/895232 for further information.

REQUIREMENTS
------------
* Entity (https://www.drupal.org/project/entity)

RECOMMENDED MODULES
-------------------
* webforms (https://www.drupal.org/project/webform)
    The webforms module should be enabled for Label Scanner to be useful,
    since currently only webform elements are scanned.

CONFIGURATION
-------------
* Configure Label Scanner user permissions in
    Administration » People » Permissions (admin/people/permissions):
    - __Administer Label Scanner__: this permission allows users to configure
    scanning options, add/edit list of restricted words, specify and configure
    notification methods.

* Configure Label scanner options
    Administration » Configure >> Content Authoring >> Label Scanner
    (admin/config/content/label_scanner):

    - __Scanning Options__: specify the automated method you would like to use
    for scanning labels.
    Currently there are 2 options:
        - Cron job: a cron job scans the webform components' labels
        - Scan labels at creation / edit time: label is validate against
        the list of restricted words when a component is created or when a label
        is modified.

    - __Restricted words__: specify a list of *restricted words*
    (i.e. words or phrases which you want to scan for).
    To enter multiple words, enter each word on a separate line.

    - __Notifications__: specify how you would like to be notified about
    an occurrence of a restricted word in a label. Two options are currently
    available. You can receive an email notification or/and display a message
    to the user in the webform UI, after they create or edit a webform component.

    __*Note:*__ For Label Scanner to work, you must configure
    __Restricted words__ and __Notifications__.
    - If you do not specify a list of *restricted words*, label scanner will
        not know which words to scan for.
    - If you do not specify a notification method, label scanner will not be
    able to deliver to you its scanning results
    (i.e. labels which contain restricted words).


USAGE
-----
This module provides different methods for scanning webform labels:

* Manual method: can be run anytime.

    - __Drush__: running the following drush command produces a report which
        displays a list of affected labels.
        `drush label-scan`


* Automated methods: must be configures through the Label Scanner configuration
    UI (see Configuration section)

    - __Cron__: once enabled through the Label Scanner configuration UI,
        under Scanning Options, cron will scan all webform labels and send
        a report by email.

    - __UI - At creation/modification__: once enabled through the Label Scanner
        configuration UI, under Scanning Options, the label will be validated at
        the time a component is created or when the label is modified.
        How you receive the report/validation result(s) will depend on
        the notification option chosen in the Labe Scanner
        __Notifications__ configuration UI.

RUNNING PHPUNIT TESTS
----------------------

* Download PHPUnit locally in the module directory. (see: https://phpunit.de/getting-started/phpunit-6.html)

`contrib/label_scanner# composer require --dev phpunit/phpunit ^6`

* Set the includePath in `tests/phpunit.xml` to your DRUPAL_ROOT.

* Run tests

`contrib/label_scanner/tests# ../vendor/bin/phpunit -v -c phpunit.xml . `

MAINTAINERS
-----------

Current maintainers:
* Karolinam (McGill) - https://www.drupal.org/u/karolinam