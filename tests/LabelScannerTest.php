<?php

/**
 * @file
 * LabelScanner unit tests.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../src/LabelScanner.php';
require_once __DIR__ . '/../src/validators/ValidatorInterface.php';
require_once __DIR__ . '/../src/components/ComponentInterface.php';
require_once __DIR__ . '/../src/components/ComponentIterator.php';
require_once __DIR__ . '/../src/notifications/MessageNotification.php';

/**
 * LabelScanner PhpUnit tests.
 */
class LabelScannerTest extends TestCase {

  /**
   * Instance of LabelScanner to use in tests.
   *
   * @var LabelScanner
   */
  private $labelScanner;

  /**
   * Stub for validator interface, passed to LabelScanner constructor.
   *
   * @var PHPUnit_Framework_MockObject_MockObject
   */
  private $validatorMock;

  /**
   * Sets up LabelScanner and WordValidator stub.
   */
  protected function setUp() {
    parent::setUp();

    $this->validatorMock = $this->createMock('ValidatorInterface');
    $this->labelScanner = new LabelScanner($this->validatorMock);
  }

  /**
   * Test isValid() when component is valid.
   *
   * @covers \LabelScanner::isValid()
   */
  public function testisValidWithValidComponent() {
    $this->validatorMock->method('isValid')->willReturn(TRUE);

    $component = $this->createConfiguredMock('ComponentInterface', array(
      'hasLabel' => TRUE,
    ));

    $this->assertTrue($this->labelScanner->isValid($component));
    // Check number of invalid components.
    $this->assertCount(0, $this->labelScanner->getIterator());

  }

  /**
   * Test isValid() when component is invalid.
   *
   * @covers \LabelScanner::isValid()
   * @covers \LabelScanner::addInvalidComponent()
   */
  public function testisValidWithInvalidComponent() {
    $this->validatorMock->method('isValid')->willReturn(FALSE);

    $component = $this->createConfiguredMock('ComponentInterface', array(
      'hasLabel' => TRUE,
    ));

    $this->assertFalse($this->labelScanner->isValid($component));
    // Check number of invalid components is 1.
    $this->assertCount(1, $this->labelScanner->getIterator());
  }

  /**
   * Test isValid() when multiple components are invalid, and a valid one.
   *
   * @covers \LabelScanner::isValid()
   * @covers \LabelScanner::getIterator()
   * @covers \LabelScanner::addInvalidComponent()
   */
  public function testisValidWithMultipleInvalidComponents() {
    $this->validatorMock->method('isValid')->will($this->onConsecutiveCalls(TRUE, FALSE, FALSE));

    $component1 = $this->createConfiguredMock('ComponentInterface', array(
      'hasLabel' => TRUE,
      'getLabel' => 'ValidLabel',
    ));
    $component2 = $this->createConfiguredMock('ComponentInterface', array(
      'hasLabel' => TRUE,
      'getLabel' => 'InvalidLabel',
    ));
    $component3 = $this->createConfiguredMock('ComponentInterface', array(
      'hasLabel' => TRUE,
      'getLabel' => 'InvalidLabel',
    ));

    $this->assertTrue($this->labelScanner->isValid($component1));
    $this->assertFalse($this->labelScanner->isValid($component2));
    $this->assertFalse($this->labelScanner->isValid($component3));
    // Check number of invalid components is 3.
    $this->assertCount(2, $this->labelScanner->getIterator());

    // Check the 2 components with invalid labels where added to LabelScanner.
    foreach ($this->labelScanner as $invalidComponent) {
      $this->assertEquals('InvalidLabel', $invalidComponent->getLabel());
    }
  }

  /**
   * Test isActive() when a LabelScanner is active.
   *
   * @covers \LabelScanner::isActive()
   * @covers \LabelScanner::attach()
   */
  public function testIsActive() {
    $this->validatorMock->method('isActive')->willReturn(TRUE);
    $notification = $this->createMock('MessageNotification');
    $this->labelScanner->attach($notification);

    $this->assertTrue($this->labelScanner->isActive());
  }

  /**
   * Test isActive() when a LabelScanner has no notifications.
   *
   * @covers \LabelScanner::isActive()
   * @covers \LabelScanner::attach()
   */
  public function testIsActiveWhenNoNotifications() {
    $this->validatorMock->method('isActive')->willReturn(TRUE);

    $this->assertFalse($this->labelScanner->isActive());
  }

  /**
   * Test isActive() when a LabelScanner has no active validator.
   *
   * @covers \LabelScanner::isActive()
   * @covers \LabelScanner::attach()
   */
  public function testIsActiveWhenValidatorNotActive() {
    $this->validatorMock->method('isActive')->willReturn(FALSE);
    $notification = $this->createMock('MessageNotification');
    $this->labelScanner->attach($notification);

    $this->assertFalse($this->labelScanner->isActive());
  }

}
