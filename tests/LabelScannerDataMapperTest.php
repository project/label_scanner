<?php

/**
 * @file
 * Contains the LabelScannerDataMapperTest class.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../src/LabelScanner.php';
require_once __DIR__ . '/../src/LabelScannerDataMapper.php';
require_once __DIR__ . '/../src/validators/ValidatorInterface.php';
require_once __DIR__ . '/../src/validators/RestrictedWordValidator.php';
require_once __DIR__ . '/../src/notifications/EmailNotification.php';
require_once __DIR__ . '/../src/notifications/MessageNotification.php';
require_once __DIR__ . '/../src/components/ComponentIterator.php';

/**
 * PhpUnit tests for LabelScannerDataMapper class.
 *
 * PHPUnit 6.2
 */
class LabelScannerDataMapperTest extends TestCase {

  /**
   * Instance of LabelScannerDataMapper to use in tests.
   *
   * @var LabelScannerDataMapper
   */
  private $dataMapper;

  /**
   * Sets up WordValidator and restricted words.
   */
  protected function setUp() {
    parent::setUp();

    $this->dataMapper = new LabelScannerDataMapper();

    // Mock variables.
    // Set restricted labels.
    $GLOBALS['conf']['label_scanner_restricted_labels'] = 'SIN
credit card
McGill ID';
    // Enable email notifications.
    $GLOBALS['conf']['label_scanner_email_notification'] = 1;

    // Set list of email to send notifications to.
    $GLOBALS['conf']['label_scanner_emails'] = 'test@test.com,another@email.com';

    // Set default mail system.
    $GLOBALS['conf']['mail_system']['default-system'] = 'DefaultMailSystem';

    // Set scanning options.
    $GLOBALS['conf']['label_scanner_options_at_update'] = '0';
    $GLOBALS['conf']['label_scanner_options_cron'] = '0';

    // Set cron interval.
    $GLOBALS['conf']['label_scanner_cron_interval'] = 'P7D';

    // Set default timezone.
    $GLOBALS['conf']['date_default_timezone'] = 'America/Toronto';
  }

  /**
   * Test getRestrictedLabels() when list of words exists..
   *
   * @covers \LabelScannerDataMapper::getRestrictedLabels()
   */
  public function testGetRestrictedLabels() {
    $result = $this->dataMapper->getRestrictedLabels();
    $this->assertEquals($result, array('SIN', 'credit card', 'McGill ID'));
  }

  /**
   * Test getRestrictedLabels().
   *
   * When the list has been previously loaded and overridden.
   *
   * @covers \LabelScannerDataMapper::setRestrictedLabels()
   * @covers \LabelScannerDataMapper::getRestrictedLabels()
   */
  public function testGetRestrictedLabelsWhenListLoaded() {

    // Load restricted labels from variable.
    $this->dataMapper->getRestrictedLabels();

    // Override restricted labels.
    $expected = array('label1, label2', 'label3');
    $this->dataMapper->setRestrictedLabels($expected);
    $result = $this->dataMapper->getRestrictedLabels();
    $this->assertEquals($result, $expected);
  }

  /**
   * Test getRestrictedLabels() when list of words is empty.
   *
   * @covers \LabelScannerDataMapper::getRestrictedLabels()
   */
  public function testGetRestrictedLabelsWhenListIsEmpty() {
    $GLOBALS['conf']['label_scanner_restricted_labels'] = NULL;
    $this->assertFalse($this->dataMapper->getRestrictedLabels());
  }

  /**
   * Test attachNotifications(), when there are no notifications configured.
   *
   * @covers \LabelScannerDataMapper::attachNotifications()
   * @covers \LabelScanner::getNotifications()
   */
  public function testAttachNotificationsNoNotifications() {
    // $stub = $this->createMock(RestrictedWordValidator::class);.
    $stub = $this->createMock('RestrictedWordValidator');

    $GLOBALS['conf']['label_scanner_email_notification'] = NULL;
    $labelScanner = new LabelScanner($stub);
    $this->dataMapper->attachNotifications($labelScanner);

    $notifications = $labelScanner->getNotifications();
    $this->assertEmpty($notifications);
  }

  /**
   * Test attachNotifications, when there is one Email notification configured.
   *
   * @covers \LabelScannerDataMapper::attachNotifications()
   * @covers \LabelScanner::getNotifications()
   * @covers \LabelScanner::attach()
   * @covers \EmailNotification()
   */
  public function testAttachNotificationsOneEmailNotification() {
    $stub = $this->createMock('RestrictedWordValidator');
    $labelScanner = new LabelScanner($stub);
    $this->dataMapper->attachNotifications($labelScanner);
    /** @var \EmailNotification $notification */
    $notification = current($labelScanner->getNotifications());
    $this->assertInstanceOf('EmailNotification', $notification);
    $this->assertEquals(explode(',', $GLOBALS['conf']['label_scanner_emails']), $notification->getEmails());
  }

  /**
   * Test attachNotifications.
   *
   * When there is an Email and Message notification configured.
   *
   * @covers \LabelScannerDataMapper::attachNotifications()
   * @covers \LabelScanner::getNotifications()
   * @covers \LabelScanner::attach()
   * @covers \EmailNotification()
   */
  public function testAttachNotificationsEmailAndMessageNotification() {
    $GLOBALS['conf']['label_scanner_ui_message_notification'] = 1;
    $stub = $this->createMock('RestrictedWordValidator');
    $labelScanner = new LabelScanner($stub);
    $this->dataMapper->attachNotifications($labelScanner);
    $notifications = $labelScanner->getNotifications();
    $this->assertInstanceOf('EmailNotification', $notifications[0]);
    $this->assertInstanceOf('MessageNotification', $notifications[1]);

  }

  /**
   * Test getValidator(), when there is a list of restricted labels configured.
   *
   * @covers \LabelScannerDataMapper::getValidator
   * @covers \LabelScannerDataMapper::getRestrictedLabels()
   */
  public function testGetValidator() {

    $validator = $this->dataMapper->getValidator();

    $this->assertInstanceOf('RestrictedWordValidator', $validator);
    $this->assertTrue($validator->isActive());
  }

  /**
   * Test getValidator(), when there is no restricted labels.
   *
   * @covers \LabelScannerDataMapper::getValidator
   * @covers \LabelScannerDataMapper::testGetRestrictedLabels
   */
  public function testGetValidatorNoRestrictedLabels() {
    // No restricted labels.
    $GLOBALS['conf']['label_scanner_restricted_labels'] = NULL;

    $validator = $this->dataMapper->getValidator();

    $this->assertInstanceOf('RestrictedWordValidator', $validator);
    $this->assertFalse($validator->isActive());
  }

  /**
   * Test getLabelScanner()
   *
   * @covers \LabelScannerDataMapper::getLabelScanner()
   */
  public function testGetLabelScanner() {
    $scanner = $this->dataMapper->getLabelScanner();

    $this->assertInstanceOf('LabelScanner', $scanner);
    $this->assertTrue($scanner->isActive());
  }

  /**
   * Test setMailSystem() when a new mail system is added.
   *
   * @covers \LabelScannerDataMapper::setMailSystem()
   */
  public function testSetMailSystem() {
    $this->dataMapper->setMailSystem('phpUnitTest', 'UnitTestingMock');
    $config = variable_get('mail_system');
    $this->assertArraySubset(array('phpUnitTest' => 'UnitTestingMock'), $config);
  }

  /**
   * Test setMailSystem() when updating a mail system.
   *
   * @covers \LabelScannerDataMapper::setMailSystem()
   */
  public function testSetMailSystemUpdate() {
    // Initial set.
    $this->dataMapper->setMailSystem('phpUnitTest', 'UnitTestingMock');
    // Update.
    $this->dataMapper->setMailSystem('phpUnitTest', 'UnitTestingMockUpdated');
    $config = variable_get('mail_system');
    $this->assertArraySubset(array('phpUnitTest' => 'UnitTestingMockUpdated'), $config);
  }

  /**
   * Test validateWithCron() when option "Run a cron job to scan labels" is
   * not enabled / not selected.
   *
   * @covers \LabelScannerDataMapper::validateWithCron()
   */
  public function testValidateWithCronIsDisabled() {
    $this->assertFalse($this->dataMapper->validateWithCron());
    // Test once data has been cached.
    $this->assertFalse($this->dataMapper->validateWithCron());

  }

  /**
   * Test validateWithCron() when option "Run a cron job to scan labels" is
   * enabled / selected.
   *
   * @covers \LabelScannerDataMapper::validateWithCron()
   */
  public function testValidateWithCronIsEnabled() {
    $GLOBALS['conf']['label_scanner_options_cron'] = 'TRUE';
    $this->assertTrue($this->dataMapper->validateWithCron());
    // Test once data has been cached.
    $this->assertTrue($this->dataMapper->validateWithCron());
  }

  /**
   * Test validateOnUpdate() when option "Scan/validate labels at creation / edit"
   * is not enabled / not selected.
   *
   * @covers \LabelScannerDataMapper::validateOnUpdate()
   */
  public function testValidateOnUpdateIsDisabled() {
    $this->assertFalse($this->dataMapper->validateOnUpdate());
    // Test once data has been cached.
    $this->assertFalse($this->dataMapper->validateOnUpdate());

  }

  /**
   * Test validateWithCron() when option "Run a cron job to scan labels" is
   * enabled / selected.
   *
   * @covers \LabelScannerDataMapper::validateOnUpdate()
   */
  public function testValidateOnUpdateIsEnabled() {
    $GLOBALS['conf']['label_scanner_options_at_update'] = 'TRUE';
    $this->assertTrue($this->dataMapper->validateOnUpdate());
    // Test once data has been cached.
    $this->assertTrue($this->dataMapper->validateOnUpdate());
  }

  /**
   * Test getEmails() when multiple emails were configured.
   *
   * @covers \LabelScannerDataMapper::getEmails()
   */
  public function testGetEmails() {
    $this->assertEquals(array('test@test.com', 'another@email.com'), $this->dataMapper->getEmails());
  }

  /**
   * Test getEmails() when no emails are configured.
   *
   * @covers \LabelScannerDataMapper::getEmails()
   */
  public function testGetEmailsWithNoEmails() {
    unset($GLOBALS['conf']['label_scanner_emails']);
    $this->assertEmpty($this->dataMapper->getEmails());
  }

  /**
   * Test getCronInterval() when interval is set to 1 day.
   *
   * @covers \LabelScannerDataMapper::getCronInterval()
   */
  public function testGetCronIntervalWhenIntervalIsOneDay() {
    $interval = $this->dataMapper->getCronInterval();
    $this->assertEquals(7, $interval->d);
  }

  /**
   * Test getCronInterval() returns default value when no interval was set.
   *
   * @covers \LabelScannerDataMapper::getCronInterval()
   */
  public function testGetCronIntervalWhenNoIntervalSet() {
    unset($GLOBALS['conf']['label_scanner_cron_interval']);
    $interval = $this->dataMapper->getCronInterval();
    $this->assertEquals(1, $interval->d);
  }

  /**
   * Test getCronLastRun() returns 0 when no value was set.
   * When cron did not yet run.
   *
   * @covers \LabelScannerDataMapper::getCronInterval()
   */
  public function testGetCronLastRunWhenCronDidNotRun() {
    unset($GLOBALS['conf']['label_scanner_cron_last_run']);
    $date = $this->dataMapper->getCronLastRun();
    $this->assertEquals(0, $date);
  }

  /**
   * Test getCronLastRun() returns DateTime object when
   * label_scanner_cron_last_run was set.
   *
   * @covers \LabelScannerDataMapper::getCronInterval()
   */
  public function testGetCronLastRunWhenCronRan() {
    $GLOBALS['conf']['label_scanner_cron_last_run'] = '1506011840';
    $date = $this->dataMapper->getCronLastRun();
    $this->assertInstanceOf('DateTime', $date);
    $this->assertEquals('2017-09-21 16:37:20', $date->format('Y-m-d H:i:s'));
  }

  /**
   * Test getCronLastRun() throws an exception when invalid timestamp was set.
   *
   * @covers \LabelScannerDataMapper::getCronInterval()
   */
  public function testGetCronLastRunWithInvalidTimestamp() {
    $GLOBALS['conf']['label_scanner_cron_last_run'] = 'xyx';
    $this->expectException('UnexpectedValueException');
    $this->dataMapper->getCronLastRun();
  }

  /**
   * Test runCron() when it's to early to run the next cron.
   *
   * This is when the interval label_scanner_cron_interval has not elapsed
   * since the last cron run.
   *
   * @covers \LabelScannerDataMapper::runCron()
   */
  public function testRunCronWhenTooEarly() {
    $GLOBALS['conf']['label_scanner_cron_interval'] = 'P2D';
    $lastRun = new DateTime('-2 hours');
    $GLOBALS['conf']['label_scanner_cron_last_run'] = $lastRun->getTimestamp();
    $this->assertFalse($this->dataMapper->runCron());
  }

  /**
   * Test runCron() when cron interval has elapsed and cron should run.
   *
   * @covers \LabelScannerDataMapper::runCron()
   */
  public function testRunCronWhenReady() {
    $GLOBALS['conf']['label_scanner_cron_interval'] = 'P1D';
    $lastRun = new DateTime('-2 days');
    $GLOBALS['conf']['label_scanner_cron_last_run'] = $lastRun->getTimestamp();
    $this->assertTrue($this->dataMapper->runCron());
  }

  /**
   * Test runCron() when it's the first cron run.
   *
   * @covers \LabelScannerDataMapper::runCron()
   */
  public function testRunCronFirstTime() {
    $GLOBALS['conf']['label_scanner_cron_interval'] = 'P1D';
    // Since cron never run before, label_scanner_cron_last_run is not set.
    unset($GLOBALS['conf']['label_scanner_cron_last_run']);
    $this->assertTrue($this->dataMapper->runCron());
  }

  /**
   * Test ScanPublishedOnly: should return false, when option was not configured/set.
   *
   * @covers \LabelScannerDataMapper::scanPublishedOnly
   */
  public function testScanPublishedOnlyNotSet() {
    $this->assertFalse($this->dataMapper->scanPublishedOnly());
  }

  /**
   * Test ScanPublishedOnly: should return true, when option selected.
   *
   * @covers \LabelScannerDataMapper::scanPublishedOnly
   */
  public function testScanPublishedOnlySelected() {
    $GLOBALS['conf']['label_scanner_options_published_only'] = TRUE;
    $this->assertTrue($this->dataMapper->scanPublishedOnly());
  }

}

/**
 * Mock variable_get() Drupal function.
 */
function variable_get($name, $default = NULL) {

  if (isset($GLOBALS['conf'][$name])) {
    return $GLOBALS['conf'][$name];
  }

  return $default;
}

/**
 * Mock variable_set() Drupal function.
 */
function variable_set($name, $value) {
  $GLOBALS['conf'][$name] = $value;
}

/**
 * Mock valid_email_address() Drupal function.
 */
function valid_email_address($email) {
  return TRUE;
}

/**
 * Mock t() Drupal function.
 */
function t($msg) {
  return $msg;
}

/**
 * Mock watchdog() Drupal function.
 */
function watchdog() {

}

define('WATCHDOG_ERROR', 'mock WATCHDOG_ERROR');
