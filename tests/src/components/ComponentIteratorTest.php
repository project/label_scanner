<?php

/**
 * @file
 * Contains the ComponentIteratorTest class.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../../src/components/ComponentIterator.php';
require_once __DIR__ . '/../../../src/components/ComponentInterface.php';
require_once __DIR__ . '/../../../src/components/WebformComponentsLoader.php';

/**
 * Unit tests for ComponentIterator.
 */
class ComponentIteratorTest extends TestCase {

  /**
   * Class under test.
   *
   * @var ComponentIterator
   */
  private $iterator;

  /**
   * Stub for WebformComponentsLoader.
   *
   * @var WebformComponentsLoader
   */
  private $loader;

  /**
   * List of components.
   *
   * @var ComponentInterface[]
   */
  private $components;

  /**
   * Sets up ComponentIterator and WebformComponentsLoader stub.
   */
  protected function setUp() {
    parent::setUp();

    $this->loader = $this->createMock('WebformComponentsLoader');

    $this->components[] = $this->createMock('ComponentInterface');
    $this->iterator = new ComponentIterator($this->components);
  }

  /**
   * Test iterator with one component.
   */
  public function testIteratorWithOneComponent() {
    $this->assertCount(1, $this->iterator);
  }

  /**
   * Test iterator with multiple components.
   */
  public function testIteratorWithMultipleComponents() {
    $this->assertCount(1, $this->iterator);
    $this->iterator->append($this->createMock('ComponentInterface'));
    $this->iterator->append($this->createMock('ComponentInterface'));
    $this->assertCount(3, $this->iterator);

  }

  /**
   * Test iterator with no component return one component.
   */
  public function testIteratorWithNoComponents() {
    $this->iterator = new ComponentIterator();
    $this->assertCount(0, $this->iterator);
  }

  /**
   * Test iterator with loader when 0 batches of components exist.
   */
  public function testIteratorWithLoaderNoBatchExists() {
    $this->iterator = new ComponentIterator();
    $this->iterator->setLoader($this->loader);
    // Loader will return only one batch with 3 components.
    $this->loader->method('loadNextBatch')->willReturn(array());

    $this->assertCount(0, $this->iterator);
  }

  /**
   * Test iterator with loader when one batch of components exists.
   */
  public function testIteratorWithLoaderOneBatch() {
    $this->iterator = new ComponentIterator();
    $this->iterator->setLoader($this->loader);
    // Loader will return only one batch with 3 components.
    $this->loader->method('loadNextBatch')
      ->will($this->onConsecutiveCalls(array('C1', 'C2', 'C3'), array()));

    $this->assertCount(3, $this->iterator);
  }

  /**
   * Test iterator with loader when multiple batches of components exists.
   */
  public function testIteratorWithLoaderMultipleBatches() {
    $this->iterator = new ComponentIterator();
    $this->iterator->setLoader($this->loader);
    // Loader will return only one batch with 3 components.
    $this->loader->method('loadNextBatch')
      ->will($this->onConsecutiveCalls(
        array('C1', 'C2'),
        array('C3', 'C4'),
        array('C5', 'C6'),
        array()));

    $this->assertCount(6, $this->iterator);
  }

}
