<?php
/**
 * @file
 * Contains the WebformEmailTest class.
 */


use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../../src/components/WebformEmailInterface.php';
require_once __DIR__ . '/../../../src/components/WebformEmail.php';
require_once 'sites/all/modules/contrib/entity/includes/entity.wrapper.inc';

/**
 * WebformEmail Unit tests.
 */
class WebformEmailTest extends TestCase {

  /**
   * @var WebformEmail
   */
  private $webform_email;

  /**
   * Mock of entity to which email belongs.
   *
   * @var EntityMetadataWrapper
   */
  private $entity;

  /**
   * Webfrom_emails data.
   *
   * @var array
   */
  private $webformEmail;

  /**
   * Sets up WebformEmail.
   */
  protected function setUp() {
    parent::setUp();

    $this->webformEmailData = array(
      'nid' => 10,
      'eid' => 2,
      'email' => 'test.domain.ca',
      'template'=> 'default',
      'excluded_components'=> '1',
    );

    $this->entity = $this->createMock('EntityMetadataWrapper');
    $this->webformEmail= new WebformEmail($this->webformEmailData, $this->entity);
  }

  /**
   * Test hasEmail .
   *
   * @covers \WebformEmail::hasEmail()
   */
  public function testHasEmail() {
    $this->assertEquals($this->webformEmailData['email'], $this->webformEmail->hasEmail());
  }


  /**
   * Test getTemplate .
   *
   * @covers \WebformEmail::getTemplate()
   */
  public function testGetTemplate() {
    $this->assertEquals($this->webformEmailData['template'], $this->webformEmail->getTemplate());
  }

  /**
   * Test getExcludedcomponents .
   *
   * @covers \WebformEmail::getExcludedcomponents()
   */
  public function testGetExcludedcomponents() {
    $this->assertEquals($this->webformEmailData['excluded_components'], $this->webformEmail->getExcludedcomponents());
  }

  /**
   * Test getEid .
   *
   * @covers \WebformEmail::getEid()
   */
  public function testGetEid() {
    $this->assertEquals($this->webformEmailData['eid'], $this->webformEmail->getEid());
  }

}
