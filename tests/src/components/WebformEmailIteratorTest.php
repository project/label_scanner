<?php

/**
 * @file
 * Contains the WebformEmailIteratorTest class.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../../src/components/WebformEmailIterator.php';
require_once __DIR__ . '/../../../src/components/WebformEmailInterface.php';
require_once __DIR__ . '/../../../src/components/WebformEmailsLoader.php';

/**
 * Unit tests for WebformEmailIterator.
 */
class WebformEmailIteratorTest extends TestCase {

  /**
   * Class under test.
   *
   * @var WebformEmailIterator
   */
  private $iterator;

  /**
   * Stub for WebformEmailsLoader.
   *
   * @var WebformEmailsLoader
   */
  private $loader;

  /**
   * List of WebformEmail.
   *
   * @var WebformEmailInterface[]
   */
  private $webform_emails;

  /**
   * Sets up WebformEmailIterator and WebformEmailsLoader stub.
   */
  protected function setUp() {
    parent::setUp();

    $this->loader = $this->createMock('WebformEmailsLoader');

    $this->webform_emails[] = $this->createMock('WebformEmailInterface');
    $this->iterator = new WebformEmailIterator($this->webform_emails);
  }

  /**
   * Test iterator with one webformemail.
   */
  public function testIteratorWithOneWebformEmails() {
    $this->assertCount(1, $this->iterator);
  }

  /**
   * Test iterator with multiple webform_emails.
   */
  public function testIteratorWithMultipleWebformEmails() {
    $this->assertCount(1, $this->iterator);
    $this->iterator->append($this->createMock('WebformEmailInterface'));
    $this->iterator->append($this->createMock('WebformEmailInterface'));
    $this->assertCount(3, $this->iterator);

  }

  /**
   * Test iterator with no WebformEmails.
   */
  public function testIteratorWithNoWebformEmails() {
    $this->iterator = new WebformEmailIterator();
    $this->assertCount(0, $this->iterator);
  }

  /**
   * Test iterator with loader when 0 batches of WebformEmails exist.
   */
  public function testIteratorWithLoaderNoBatchExists() {
    $this->iterator = new WebformEmailIterator();
    $this->iterator->setLoader($this->loader);
    // Loader will return only one batch with 3 webform_emails.
    $this->loader->method('loadNextBatch')->willReturn(array());

    $this->assertCount(0, $this->iterator);
  }

  /**
   * Test iterator with loader when one batch of webform_emails exists.
   */
  public function testIteratorWithLoaderOneBatch() {
    $this->iterator = new WebformEmailIterator();
    $this->iterator->setLoader($this->loader);
    // Loader will return only one batch with 3 WebformEmails.
    $this->loader->method('loadNextBatch')
      ->will($this->onConsecutiveCalls(array('C1', 'C2', 'C3'), array()));

    $this->assertCount(3, $this->iterator);
  }

  /**
   * Test iterator with loader when multiple batches of WebformEmails exists.
   */
  public function testIteratorWithLoaderMultipleBatches() {
    $this->iterator = new WebformEmailIterator();
    $this->iterator->setLoader($this->loader);
    // Loader will return only one batch with 3 WebformEmails.
    $this->loader->method('loadNextBatch')
      ->will($this->onConsecutiveCalls(
        array('C1', 'C2'),
        array('C3', 'C4'),
        array('C5', 'C6'),
        array()));

    $this->assertCount(6, $this->iterator);
  }

}
