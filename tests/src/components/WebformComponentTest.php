<?php

/**
 * @file
 * Contains the WebformComponentTest class.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../../src/components/ComponentInterface.php';
require_once __DIR__ . '/../../../src/components/WebformComponent.php';
require_once 'sites/all/modules/contrib/entity/includes/entity.wrapper.inc';

/**
 * WebformComponent Unit tests.
 */
class WebformComponentTest extends TestCase {

  /**
   * @var WebformComponent
   */
  private $component;

  /**
   * Mock of entity to which component belongs.
   *
   * @var EntityMetadataWrapper
   */
  private $entity;

  /**
   * Webfrom component data.
   *
   * @var array
   */
  private $componentData;

  /**
   * Sets up WebformComponent.
   */
  protected function setUp() {
    parent::setUp();

    $this->componentData = array(
      'name' => 'Label A',
      'nid' => 10,
      'cid' => 5,
      'type' => 'textfield',
      'form_key' => 'label_a',
    );

    $this->entity = $this->createMock('EntityMetadataWrapper');
    $this->component = new WebformComponent($this->componentData, $this->entity);
  }

  /**
   * Test getComponentType .
   *
   * @covers \WebformComponent::getComponentType()
   */
  public function testGetComponentType() {
    $this->assertEquals($this->componentData['type'], $this->component->getComponentType());
    ;
  }

  /**
   * Test getNodeId when one exists.
   *
   * @covers \WebformComponent::getNodeId()
   */
  public function testGetNodeId() {
    $this->assertEquals($this->componentData['nid'], $this->component->getNodeId());
  }

  /**
   * Test getComponentId when one exists.
   *
   * @covers \WebformComponent::getComponentId()
   */
  public function testGetComponentId() {
    $this->assertEquals($this->componentData['cid'], $this->component->getComponentId());
  }

  /**
   * Test hasLabel when component has a label.
   *
   * @covers \WebformComponent::hasLabel()
   */
  public function testhasLabel() {
    $GLOBALS['webform_component_feature'][$this->componentData['type']]['title'] = TRUE;
    $this->assertTrue($this->component->hasLabel());
  }

  /**
   * Test hasLabel when component has an empty label.
   *
   * @covers \WebformComponent::hasLabel()
   */
  public function testhasLabelComponentWithEmptyLabel() {
    $GLOBALS['webform_component_feature'][$this->componentData['type']]['title'] = TRUE;
    $this->componentData['name'] = '';
    $this->component = new WebformComponent($this->componentData, $this->entity);
    $this->assertFalse($this->component->hasLabel());
  }

  /**
   * Test hasLabel when component has no lable.
   *
   * @covers \WebformComponent::hasLabel()
   */
  public function testhasLabelComponentNoLabel() {
    $GLOBALS['webform_component_feature'][$this->componentData['type']]['title'] = FALSE;
    $this->assertFalse($this->component->hasLabel());
  }

}

/**
 * Mock webform_component_feature() Drupal contrib function.
 */
function webform_component_feature($type, $feature) {
  return $GLOBALS['webform_component_feature'][$type][$feature];
}
