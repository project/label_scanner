<?php

/**
 * @file
 * Contains AllowedDomainValidatorTest class.
 *
 * Allowed domain validator validator unit tests.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../../src/validators/ValidatorInterface.php';
require_once __DIR__ . '/../../../src/validators/AllowedDomainValidator.php';
require_once __DIR__ . '/../../../src/components/WebformEmail.php';
require_once 'sites/all/modules/contrib/entity/includes/entity.wrapper.inc';

/**
 * PhpUnit tests for AllowedDomainValidator class.
 *
 * PHPUnit 6.2
 */
class AllowedDomainValidatorTest extends TestCase {


  /**
   * Validator object.
   *
   * @var AllowedDomainValidator
   */
  private $validator;

  /**
   * List of allowed domains. Domains which can be used in email.
   *
   * @var array
   */
  private $alloweddomain = array();

  /**
   * Sets up AllowedDomainValidator and allowed domains.
   */
  protected function setUp() {
    parent::setUp();

    // Get configured allowed domain.
    $config_str = 'domain.ca
    campus.domain.ca
    campus2.domain.ca
    campus3.domain.ca
    ';
    $this->emailData = array(
      'email'=>'test@domain.ca',
      'template'=> 'default',
      'attachments'=> 0,
    );

    $this->entity = $this->createMock('EntityMetadataWrapper');
    $this->email = new WebformEmail($this->emailData, $this->entity);
    $this->alloweddomain = explode(PHP_EOL, $config_str);
    $this->validator = new AllowedDomainValidator($this->alloweddomain);


  }

  /**
   * Set allowed domains with none empty list.
   *
   * @covers \AllowedDomainValidator::setAllowedDomains()
   */
  public function testSetAllowedDomains() {
    $this->validator = new AllowedDomainValidator(array('domain.ca', 'campus.domain.ca'));
    $this->assertTrue($this->validator->isActive());
    $this->assertTrue($this->validator->isValid( $this->email));
  }

  /**
   * Set allowed domains with an empty  list.
   *
   * @covers \AllowedDomainValidator::setAllowedDomains()
   */
  public function testSetEmptyAllowedDomains() {
    $this->validator = new AllowedDomainValidator(array('', ''));
    $this->assertFalse($this->validator->isActive());
  }

  /**
   * Replace initial list with a new list of allowed domains.
   *
   * @covers \AllowedDomainValidator::setAllowedDomains()
   */
  public function testReplaceAllowedDomains() {

    $this->validator->setAllowedDomains(array('test.ca', 'campus.test.ca'));
    $this->assertTrue($this->validator->isActive());
    $this->entity = $this->createMock('EntityMetadataWrapper');
    $this->emailDataNew = array(
      'email'=>'test@test.ca',
      'template'=> 'default',
      'attachments'=>""
    );
   $emailNew = new WebformEmail($this->emailDataNew, $this->entity);
    // not allowed domain previously should now be valid.
   $this->assertTrue($this->validator->isValid($emailNew));
    // previously allowed domain should not be valid (should return the reason).
   $this->assertContains($this->validator->isValid($this->email ), ["default template", "submission", "attachments"]);
  }

  /**
   * Test isValid with  allowed domains.
   *
   * @dataProvider validDomainProvider
   */
  public function testIsValidWithAllowedDomain($allowed) {
    $this->assertTrue($this->validator->isValid($this->email), ' "' . $allowed . '" should have passed validation, but did not.');
  }

  /**
   * Test isValid with  not allowed domains.
   *
   * @dataProvider invalidDomainProvider
   */
  public function testIsValidWithNotAllowedDomain($domain) {
    $this->assertContains($this->validator->isValid($this->email),  ["default template", "submission", "attachments"], $domain . '" should fail validation but did not');
  }

  /**
   * Data provider for testIsValidWithAllowedDomain().
   *
   * @return array
   *    List of allowed domain.
   */
  public function validDomainProvider() {
    return array(
      array('domain.ca'),
      array('campus.domain.ca'),
      array('campus2.domain.ca'),
    );
  }


  /**
   * Data provider for testIsValidWithNotAllowedDomain().
   *
   * @return array
   *    List of not allowed domain.
   */
  public function invalidDomainProvider() {
    return array(
      array('domain.ca'),
      array('gmail.ca'),
      array('hotmail.com'),
      array('yahoo.com'),
      array('yahoo.ca'),
    );
  }
}
