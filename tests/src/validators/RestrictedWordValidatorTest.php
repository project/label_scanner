<?php

/**
 * @file
 * Contains RestrictedWordValidatorTest class.
 *
 * Restricted Word validator unit tests.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../../../src/validators/ValidatorInterface.php';
require_once __DIR__ . '/../../../src/validators/RestrictedWordValidator.php';

/**
 * PhpUnit tests for RestrictedWordValidator class.
 *
 * PHPUnit 6.2
 */
class RestrictedWordValidatorTest extends TestCase {


  /**
   * Validator object.
   *
   * @var RestrictedWordValidator
   */
  private $validator;

  /**
   * List of restricted words. Words which should not be used in labels.
   *
   * @var array
   */
  private $restrictedWords = array();

  /**
   * Sets up WordValidator and restricted words.
   */
  protected function setUp() {
    parent::setUp();

    // Get configured restricted words.
    $config_str = 'account number
cardholder name
carte d’étudiant
carte d\'étudiant
citizenship
citoyenneté
code de service
conduire
credit
crédit
date d’expiration
date d\'expiration
debit
débit
drivers
driver’s
driver\'s
drivers’
drivers\'
expiration date
license
mot de passe
nom d’utilisateur
nom d\'utilisateur
nom du titulaire
numéro de compte
passeport
passport
password
permis
RAMQ
sin
student id
username
user name
matricule
CID
CVV
CVV2';
    $this->restrictedWords = explode(PHP_EOL, $config_str);

    $this->validator = new RestrictedWordValidator($this->restrictedWords);
  }

  /**
   * Set restricted words with none empty list.
   *
   * @covers \RestrictedWordValidator::setRestrictedWords()
   */
  public function testSetRestrictedWords() {
    $this->validator = new RestrictedWordValidator(array('word1', 'word2'));
    $this->assertTrue($this->validator->isActive());
    $this->assertFalse($this->validator->isValid('word2'));
  }

  /**
   * Set restricted words with an empty word list.
   *
   * @covers \RestrictedWordValidator::setRestrictedWords()
   */
  public function testSetEmptyRestrictedWords() {
    $this->validator = new RestrictedWordValidator(array('', ''));
    $this->assertFalse($this->validator->isActive());
  }

  /**
   * Replace initial list with a new list of restricted words.
   *
   * @covers \RestrictedWordValidator::setRestrictedWords()
   */
  public function testReplaceRestrictedWords() {

    $this->validator->setRestrictedWords(array('new word'));
    $this->assertTrue($this->validator->isActive());
    // Word from initial list should now be valid.
    $this->assertTrue($this->validator->isValid('password'));
    // Word from new list should not be valid.
    $this->assertFalse($this->validator->isValid('new word'));
  }

  /**
   * Test isValid with a valid word.
   *
   * @dataProvider validWordsProvider
   */
  public function testIsValidWithValidWord($validWord) {
    $this->assertTrue($this->validator->isValid($validWord), ' "' . $validWord . '" should have passed validation, but did not.');
  }

  /**
   * Test isValid with a restricted word.
   *
   * @dataProvider invalidWordsProvider
   */
  public function testIsValidWithRestrictedWord($word) {
    $this->assertFalse($this->validator->isValid($word), ' "' . $word . '" should fail validation but did not');
  }

  /**
   * Data provider for testIsValidWithValidWord().
   *
   * @return array
   *    List of valid words.
   */
  public function validWordsProvider() {
    return array(
      array('Name'),
      array('title'),
      // contains the restricted work "sin" in "advising".
      array('Are you interested to join the advising board?'),
    );
  }

  /**
   * Data provider for testIsValidWithRestrictedWord().
   *
   * @return array
   *    List of invalid words.
   */
  public function invalidWordsProvider() {
    return array(
      array('SIN'),
      array('sin'),
      array('sIn'),
      array('Please give me your credit card'),
      array('Payer avec votre carte de crédit.'),
      array('Card validation (cvv2)'),
      array('Copy of your driver’s license'), // right single quotation mark (U+2019)
      array("Valid ID example: driver's license"), // apostrophe (U+0027)
      array('Enter your user name'),
      array('Enter your username'),
      array('How many passports do you have?'), // plural of passport
    );
  }

}
