<?php

/**
 * @file
 * Administration page callbacks for the label scanner module.
 */

/**
 * Label Scanner administration pages.
 *
 * Allows to add/modify a list of restricted words and notification settings.
 */
function label_scanner_admin_list($form, &$form_state) {
  $form = array();

  $form['label_scanner_restricted_labels'] = array(
    '#type' => 'textarea',
    '#title' => t('List of words which should not be used in labels'),
    '#description' => t("Enter a word or phrase you want to add to this list. You can enter multiple words by adding each words on a new line. Validation is case insensitive. No need to enter the words in lower and upper case."),
    '#default_value' => variable_get('label_scanner_restricted_labels', NULL),
  );

  return system_settings_form($form);
}

/**
 * Allows to add/modify a list of allowed domain.
 */
function label_scanner_admin_domain($form, &$form_state) {
  $form = array();

  $form['label_scanner_allowed_domains'] = array(
    '#type' => 'textarea',
    '#title' => t('List of domain names where the information can be sent'),
    '#description' => t("Enter a word or phrase you want to add to this list. You can enter multiple words by adding each words on a new line. Validation is case insensitive. No need to enter the words in lower and upper case."),
    '#default_value' => variable_get('label_scanner_allowed_domains'),
  );

  return system_settings_form($form);
}
/**
 * Label Scanner administration page for setting scanning options.
 */
function label_scanner_admin_options($form, &$form_state) {
  $form = array();

  $form['label_scanner_options_at_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Scan / validate labels at creation / edit (when a label gets created or updated).'),
    '#description' => t('When a component is modified, a label will be validated only if it\'s value has changed. This will avoid being spammed with notifications each time a component is modified.'),
    '#default_value' => variable_get('label_scanner_options_at_update', 0),
    '#return_value' => TRUE,
    '#prefix' => '<div>',
  );

  $form['label_scanner_options_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Run a cron job to scan labels.'),
    '#default_value' => variable_get('label_scanner_options_cron', 0),
    '#description' => t(''),
    '#return_value' => TRUE,
    '#suffix' => '</div>',
  );

  $form['label_scanner_options_published_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Scan only published nodes.'),
    '#default_value' => variable_get('label_scanner_options_published_only', 0),
    '#description' => t(''),
    '#return_value' => TRUE,
    '#suffix' => '</div>',
  );

  return system_settings_form($form);
}

/**
 * Label Scanner administration page for setting notification methods.
 */
function label_scanner_admin_notification($form, &$form_state) {
  $form = array();

  $form['label_scanner_notification_type_email'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['label_scanner_notification_type_email']['label_scanner_email_notification'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send me notifications by email.'),
    '#default_value' => variable_get('label_scanner_email_notification', NULL),
    '#return_value' => TRUE,
  );

  $form['label_scanner_notification_type_email']['label_scanner_emails'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail addresses'),
    '#description' => t('Enter one or more email addresses to which notifications should be sent when a restricted word is detected in a label. To enter multiple e-mail addresses seperate them with a comma (,).'),
    '#default_value' => variable_get('label_scanner_emails', NULL),
    '#element_validate' => array('label_scanner_validate_emails'),
  );

  $form['label_scanner_notification_type_ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Interface'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['label_scanner_notification_type_ui']['label_scanner_ui_message_notification'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display message in the admin user interface.'),
    '#default_value' => variable_get('label_scanner_ui_message_notification', NULL),
    '#return_value' => TRUE,
  );

  $form['#submit'][] = 'label_scanner_admin_notification_submit';

  return system_settings_form($form);
}

/**
 * Form submission handler for label_scanner_admin_notification().
 */
function label_scanner_admin_notification_submit($form, &$form_state) {
  // Set label scanner mail system if email notifications are selected.
  if ($form_state['values']['label_scanner_email_notification']) {
    $data_mapper = new LabelScannerDataMapper();
    $data_mapper->setMailSystem('label_scanner_invalid_label', 'HTMLMailSystem');
  }
}

/**
 * Validation function for email addresses, for element label_scanner_emails.
 */
function label_scanner_validate_emails($element, &$form_state) {
  if (!empty($element['#value'])) {
    $emails = array_map('trim', explode(',', $element['#value']));
    if (is_array($emails) && !empty($emails)) {
      foreach ($emails as $one_email) {
        if (!valid_email_address($one_email)) {
          form_error($element, t('%email is not a valid e-mail address.', array('%email' => $one_email)));
        }
      }
    }
  }
}
