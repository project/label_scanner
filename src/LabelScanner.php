<?php

/**
 * @file
 * Contains LabelScanner class.
 */

/**
 * Scans component(s) labels and sends notifications.
 *
 * Given a validator and notifications, allows to validate component(s)' labels and
 * send notifications.
 *
 * Implements the Observer Design Pattern.
 * Implements the IteratorAggregate interface to be able to iterate over invalid
 * components.
 */
class LabelScanner implements \SplSubject, \IteratorAggregate {

  /**
   * Validator object.
   *
   * @var \ValidatorInterface
   */
  private $validator;

  /**
   * List of enabled notifications.
   *
   * @var \SplObserver[]
   */
  private $notifications;

  /**
   * What has been scanned . (Label/Email)
   *
   * @var \SplObserver[]
   */
  private $scannername;

  /**
   * Component which label is to be validated.
   *
   * @var ComponentIterator
   */
  private $invalidComponents = array();

  /**
   * Component which label is to be validated.
   *
   * @var WebformEmailIterator
   */
  private $invalidWebformEmail = array();

  /**
   * LabelScanner constructor.
   *
   * @param ValidatorInterface $validator
   *   Validator object.
   */
  public function __construct(ValidatorInterface $validator) {
    $this->validator = $validator;
    $this->invalidComponents = new ComponentIterator();
    $this->invalidWebformEmail = new WebformEmailIterator();
  }

  /**
   * Add notification.
   *
   * @param \SplObserver $observer
   *   Notification to attach.
   */
  public function attach(SplObserver $observer) {
    $this->notifications[] = $observer;
  }

  /**
   * Remove notification.
   *
   * @param \SplObserver $observer
   *   Notification to detach.
   */
  public function detach(SplObserver $observer) {
    $key = array_search($observer, $this->notifications, TRUE);
    if ($key) {
      unset($this->notifications[$key]);
    }
  }

  /**
   * Get list of notification objects.
   *
   * @return array\SplObserver[]
   *   Returns the notification.
   */
  public function getNotifications() {
    return $this->notifications;
  }

  /**
   * Get list of notification objects.
   *
   * @return array\SplObserver[]
   *   Returns scannername.
   */
  public function getScannername() {
    return $this->scannername;
  }

  /**
   * Send all notifications.
   */
  public function notify($scanner = "Label") {
    // Do not send notifications when there are no invalid components.
    if ((count($this->invalidComponents) < 1) && (count($this->invalidWebformEmail) < 1)) {
      return FALSE;
    }
    $this->scannername = $scanner;

    foreach ($this->notifications as $notification) {
      /** @var  \SplObserver  $notification */
      $notification->update($this);
    }
  }

  /**
   * Validates component's label.
   *
   * @param \ComponentInterface $component
   *   Component to validate.
   *
   * @return bool
   *   TRUE if component's label is valid.
   */
  public function isValid(\ComponentInterface $component) {
    if ($component->hasLabel()) {
      $isValid = $this->validator->isValid($component->getLabel());
      if (!$isValid) {
        $this->addInvalidComponent($component);
      }
      return $isValid;
    }

    // @todo: what to return when component has no label?
  }

  /**
   * Validates emails's domain name.
   *
   * @param \WebformEmailInterface $component
   *   Component to validate.
   *
   * @return bool
   *   TRUE if component's email is valid.
   */
  public function isValidDomain(\WebformEmailInterface $component) {
    if ($component->hasEmail()) {
      $isValid = $this->validator->isValid($component);
      if ($isValid !== TRUE) {
        $component->setReason($isValid);
        $this->addInvalidDomain($component);
      }
    }
    return $isValid;
  }

  /**
   * Checks if label scanner is active.
   *
   * Label scanner is considered active if both the validator and notifications
   * are set and valid.
   * (There is no point of running validation if a validator is not configured
   * or if no notifications will be sent out.)
   */
  public function isActive() {
    return $this->validator->isActive() && !empty($this->notifications);
  }

  /**
   * Add a component which has not passed validation.
   *
   * (Components are needed for sending notifications.)
   *
   * @param \ComponentInterface $component
   *   Component which failed validation.
   */
  public function addInvalidComponent(\ComponentInterface $component) {
    $this->invalidComponents->append($component);
  }

  /**
   * Add a component which has not passed validation.
   *
   * (Components are needed for sending notifications.)
   *
   * @param \WebformEmailInterface $component
   *   Component which failed validation.
   */
  public function addInvalidDomain(\WebformEmailInterface $component) {
    $this->invalidWebformEmail->append($component);
  }

  /**
   * Get Iterator.Allows to iterate over invalid components.
   *
   * @see IteratorAggregate::getIterator()
   *
   * @return \ComponentIterator
   *   or \WebformEmailIterator
   */
  public function getIterator() {
    return ($this->validator instanceof AllowedDomainValidator) ? $this->invalidWebformEmail : $this->invalidComponents;
  }

}
