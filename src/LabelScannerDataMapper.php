<?php

/**
 * @file
 * Contains LabelScannerDataMapper class.
 */

/**
 * Class LabelScannerDataMapper is the data layer for label scanner module.
 *
 * Retrieves data required to instantiate objects and creates instance
 * of those objects.
 */
class LabelScannerDataMapper {

  /**
   * List of restricted words, which should not be used in labels.
   *
   * @var null|false|array
   *    null if list has not been loaded
   *    False if an invalid list was loaded
   *    array list of restricted words
   */
  private $restrictedLabels = NULL;

  private $notifications = NULL;

  private $allowedDomains = NULL;

  /**
   * Scanning options.
   *
   * Options are configurable through the admin UI.
   *
   * @var arraylistofscanningoptions
   */
  private $options = array();

  /**
   * LabelScannerDataMapper constructor.
   */
  public function __construct() {
  }

  /**
   * Set restricted labels.
   *
   * Allows to override the list of loaded restricted labels.
   *
   * @param array $restrictedLabels
   *    List of words which should not be used as labels.
   *
   * @see getRestrictedLabels()
   */
  public function setRestrictedLabels($restrictedLabels) {
    $this->restrictedLabels = $restrictedLabels;
  }

  /**
   * Loads and formats list of restricted labels.
   *
   * @return array
   *      list of restricted labels or FALSE if a valid list does not exist.
   */
  public function getRestrictedLabels() {

    // If list was previously loaded, return loaded list.
    if (!is_null($this->restrictedLabels)) {
      return $this->restrictedLabels;
    }

    // Get list of restricted words.
    $words = variable_get('label_scanner_restricted_labels');

    if (empty($words)) {
      $this->restrictedLabels = FALSE;
    }
    else {
      $this->restrictedLabels = array_map('trim', explode(PHP_EOL, $words));
    }

    return $this->restrictedLabels;
  }
  /**
   * Set allowed Domain.
   *
   * Allows to override the list of loaded allowed domain.
   *
   * @param array $allowedDomains
   *    List of allowed domain where the form data can be sent.
   *
   * @see getAllowedDomains()
   */
  public function setAllowedDomains($allowedDomains) {
    $this->allowedDomains = $allowedDomains;
  }

  /**
   * Loads and formats list of allowed domain.
   *
   * @return array
   *      list of allowed domains or FALSE if a valid list does not exist.
   */
  public function getAllowedDomains() {

    // If list was previously loaded, return loaded list.
    if (!is_null($this->allowedDomains)) {
      return $this->allowedDomains;
    }

    // Get list of restricted words.
    $words = variable_get('label_scanner_allowed_domains');

    if (empty($words)) {
      $this->allowedDomains = FALSE;
    }
    else {
      $this->allowedDomains = array_map('trim', explode(PHP_EOL, $words));
    }

    return $this->allowedDomains;
  }

  /**
   * Loads and extracts list of emails.
   *
   * @return array
   *   List of emails. An empty array if none where configured.
   */
  public function getEmails() {
    if (variable_get('label_scanner_email_notification', FALSE)) {
      $emailsString = variable_get('label_scanner_emails');
      if (strlen($emailsString) > 0) {
        return array_map('trim', explode(',', $emailsString));
      }
    }

    return array();
  }

  /**
   * Get interval at which cron should validate labels.
   *
   * Value is stored in variable label_scanner_cron_interval.
   * Default interval value is 1 day.
   *
   * @return \DateInterval
   *
   * @throws \Exception
   */
  public function getCronInterval() {
    // Interval spec (value from variable label_scanner_cron_interval) should be
    // in the format as expected by php DateInterval class.
    // example: 1 day will be 'P1D'.
    // Where letter P is for "period", and D for "days".
    $intervalSpec = variable_get('label_scanner_cron_interval', 'P1D');
    return new DateInterval($intervalSpec);
  }

  /**
   * Get DateTime when cron last run.
   *
   * @return bool|\DateTime
   *   Date and time when cron last ran,
   *   0 if cron has not run / label_scanner_cron_last_run was not set.
   *   FALSE on error.
   */
  public function getCronLastRun() {
    $timestamp = variable_get('label_scanner_cron_last_run', 0);
    if (!is_numeric($timestamp)) {
      throw new UnexpectedValueException(t('Variable label_scanner_cron_last_run is not a valid timestamp.'));
    }

    $timestamp = (int) $timestamp;
    if ($timestamp == 0) {
      return $timestamp;
    }

    // Uses UTC.
    $date = new DateTime();
    if ($date->setTimestamp($timestamp)) {
      return $date;
    }

    return FALSE;
  }

  /**
   * Set timestamp of last cron run.
   *
   * @param int $timestamp UTC timestamp.
   */
  public function setCronLastRun($timestamp = 0) {
    if (is_integer($timestamp)) {
      variable_set('label_scanner_cron_last_run', $timestamp);
    }
  }

  /**
   * Checks if validation should be run.
   *
   * Validation through cron should be performed only at specified intervals.
   *
   * @see \LabelScannerDataMapper::getCronInterval()
   *
   * @return bool
   */
  public function runCron() {
    $lastCronDate = $this->getCronLastRun();

    if ($lastCronDate === 0) {
      // Cron never ran. It should run.
      return TRUE;
    }
    elseif ($lastCronDate === FALSE) {
      // An error occurred.
      return FALSE;
    }

    $interval = $this->getCronInterval();
    $now = new DateTime();
    $nextCronDate = clone $lastCronDate;
    $nextCronDate->add($interval);
    if ($now >= $nextCronDate) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks validation with cron is enabled.
   *
   * If scanning option "Run a cron job to scan labels" is selected.
   *
   * @return bool
   *   TRUE if enable otherwise FALSE.
   */
  public function validateWithCron() {
    if (isset($this->options['label_scanner_options_cron']) && is_bool($this->options['label_scanner_options_cron'])) {
      return $this->options['label_scanner_options_cron'];
    }

    $this->options['label_scanner_options_cron'] = (bool) variable_get('label_scanner_options_cron', FALSE);
    return $this->options['label_scanner_options_cron'];
  }

  /**
   * Check if validation at creation/update is enabled.
   *
   * @return bool
   *   TRUE if enable otherwise FALSE.
   */
  public function validateOnUpdate() {
    if (isset($this->options['label_scanner_options_at_update']) && is_bool($this->options['label_scanner_options_at_update'])) {
      return $this->options['label_scanner_options_at_update'];
    }

    $this->options['label_scanner_options_at_update'] = (bool) variable_get('label_scanner_options_at_update', FALSE);
    return $this->options['label_scanner_options_at_update'];

  }

  /**
   * Check if validation of email data at creation/update is enabled.
   *
   * @return bool
   *   TRUE if enable otherwise FALSE.
   */
  public function validateEmailDataOnUpdate() {
    if (isset($this->options['label_scanner_options_email_data_at_update']) && is_bool($this->options['label_scanner_options_email_data_at_update'])) {
      return $this->options['label_scanner_options_email_data_at_update'];
    }

    $this->options['label_scanner_options_email_data_at_update'] = (bool) variable_get('label_scanner_options_email_data_at_update', FALSE);
    return $this->options['label_scanner_options_email_data_at_update'];
  }

  /**
   * Check if only published nodes should be scanned.
   *
   * @return bool TRUE if only published nodes should be scanned. FALSE otherwise.
   */
  public function scanPublishedOnly() {
    if (isset($this->options['label_scanner_options_published_only']) && is_bool($this->options['label_scanner_options_published_only'])) {
      return $this->options['label_scanner_options_published_only'];
    }

    $this->options['label_scanner_options_published_only'] = (bool) variable_get('label_scanner_options_published_only', FALSE);
    return $this->options['label_scanner_options_published_only'];
  }

  /**
   * Set mailing system for a module.
   *
   * @param string $name
   *   mails system name/key. Can be {$module}_{$key}.
   * @param string $mail_system
   *   name of class which implements the MailSystemInterface interface.
   */
  public function setMailSystem($name, $mail_system) {
    $var = variable_get('mail_system', array());
    // Checks if a mail system has been configured or if a mail system
    // is being updated.
    if (!isset($var[$name]) || (!empty($var[$name]) && $var[$name] != $mail_system)) {
      $var[$name] = $mail_system;
      // Make sure default system is present.
      $var['default-system'] = 'DefaultMailSystem';
      variable_set('mail_system', $var);
    }
  }

  /**
   * Unset / Remove specific mail system.
   *
   * @param string $name
   *   mails system name/key. Can be {$module}_{$key}.
   */
  public function deleteMailSystem($name) {
    $var = variable_get('mail_system', array());
    if (isset($var[$name])) {
      unset($var[$name]);
      // Make sure default system is present.
      $var['default-system'] = 'DefaultMailSystem';
      variable_set('mail_system', $var);
    }
  }

  /**
   * Attach notifications to label scanner.
   *
   * @return LabelScanner
   *    LabelScanner object.
   */
  public function attachNotifications(LabelScanner $scanner) {

    // E-mail notification.
    try {
      $emailNotification = new EmailNotification($this->getEmails());
      $this->notifications[] = $emailNotification;
      $scanner->attach($emailNotification);
    }
    catch (UnexpectedValueException $e) {
      watchdog('label_scanner', 'Exception UnexpectedValueException: %msg. Trace: %trace', array(
        '%msg' => $e->getMessage(),
        '%trace' => $e->getTraceAsString(),
      ), WATCHDOG_ERROR);
    }

    // UI message notification.
    if (variable_get('label_scanner_ui_message_notification', FALSE)) {
      $notification = new MessageNotification();
      $this->notifications[] = $notification;
      $scanner->attach($notification);
    }

    return $scanner;
  }

  /**
   * Get validator.
   *
   * @return \ValidatorInterface
   *    ValidatorInterface object.
   */
  public function getValidator() {
    // @todo: possible future improvement: allow user to select validation type
    // in label scanner configuration and return the appropriate validator.
    return new RestrictedWordValidator($this->getRestrictedLabels());
  }

  /**
   * Get validator.
   *
   * @return \ValidatorInterface
   *    ValidatorInterface object.
   */
  public function getDomainValidator() {
    // @todo: possible future improvement: allow user to select validation type
    // in label scanner configuration and return the appropriate validator.
    return new AllowedDomainValidator($this->getAllowedDomains());
  }

  /**
   * Builds label scanner according to config settings.
   *
   * Loads appropriate validator, restricted labels and attaches notifications.
   *
   * @return \LabelScanner
   *    LabelScanner object.
   */
  public function getLabelScanner() {
    return $this->attachNotifications(new LabelScanner($this->getValidator()));
  }

}
