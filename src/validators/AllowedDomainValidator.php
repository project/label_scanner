<?php

/**
 * @file
 * Contains the AllowedDomainValidator class.
 */

/**
 * Given a list of domain name, validates emails domain.
 *
 * Checks for invalid email domains.
 */
class AllowedDomainValidator implements ValidatorInterface {
  /**
   * Webform email allowed domain.
   *
   * @var array
   */
  private $allowedDomains = array();

  /**
   * WordValidator constructor.
   *
   * @param array $allowedDomains
   *   List of allowed domain.
   */
  public function __construct(array $allowedDomains) {
    $this->setAllowedDomains($allowedDomains);
  }

  /**
   * Checks if $webform_email is in the allowed domain list.
   *
   * @param WebformEmailInterface $webform_email
   *   Test to validate.
   *
   * @return bool
   *   True if domain is valid, email information if email is not in the allowed domain list.
   */
  public function isValid($webform_email) {
    if (empty($this->allowedDomains)) {
      return TRUE;
    }
    $found = FALSE;
    $emails = explode(",", $webform_email->getEmail());
    foreach ($emails as $email) {
      if (is_numeric($email)) {
        // Webform component "email" field.
        $found = TRUE;
      }
      else {
        $domain = explode('@', $email, 2)[1];
        if (!in_array(strtolower($domain), array_map('strtolower', $this->allowedDomains))) {
          $found = TRUE;
        }
      }
    }
    $template = $webform_email->getTemplate();

    if (!$found) {
      return TRUE;
    }

    if ($template === 'default') {
      $return = "default template";
    }
    else {
      $return = "";
      if (stripos($template, "[submission:values") !== FALSE) {
        $return .= "[submission:values]";
      }
      if (stripos($template, "[submission:user") !== FALSE) {
        $return .= "[submission:user]";
      }
      if (stripos($template, "[submission:referenced-values") !== FALSE) {
        $return .= "[submission:referenced-values]";
      }
      if (stripos($template, "[submission:access-token]") !== FALSE) {
        $return .= "[submission:access-token]";
      }
      if ($webform_email->getAttachments() == TRUE) {
        $return .= "attachment";
      }
    }
    return empty($return) ? TRUE : $return;
  }

  /**
   * Check if validator is active.
   *
   * Validator is active if it has the required data to perform validation.
   *
   * @return bool
   *   TRUE if allowed domains are set.
   */
  public function isActive() {
    return !empty($this->allowedDomains);
  }

  /**
   * Set / Replace a list of allowed domains.
   *
   * @param array $allowedDomains
   *   Set allowedDomains.
   */
  public function setAllowedDomains(array $allowedDomains) {
    $newWords = array();
    if (!empty($allowedDomains) && is_array($allowedDomains)) {
      foreach ($allowedDomains as $word) {
        if (strlen($word) > 0) {
          $newWords[] = $word;
        }
      }

      $this->allowedDomains = $newWords;
    }
  }

}
