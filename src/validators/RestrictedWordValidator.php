<?php

/**
 * @file
 * Contains the RestrictedWordValidator class.
 */

/**
 * Given a list of restricted words, validates input text.
 *
 * Checks if input text contains any of the restricted words.
 */
class RestrictedWordValidator implements ValidatorInterface {

  private $restrictedWords = array();

  /**
   * WordValidator constructor.
   *
   * @param array $restrictedWords
   *    List of restricted words.
   */
  public function __construct($restrictedWords) {
    $this->setRestrictedWords($restrictedWords);
  }

  /**
   * Checks if $text is in the restricted list.
   *
   * @param string $text
   *      Test to validate.
   *
   * @return bool
   *    True if word is valid, false if word is in the restricted list.
   */
  public function isValid($text) {

    if (empty($this->restrictedWords) || empty($text)) {
      return TRUE;
    }

    foreach ($this->restrictedWords as $restrictedWord) {
      // Check if text contains any of the restricted words.
      if (preg_match("/\b${restrictedWord}(e?s)?\b/i", $text)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Check if validator is active.
   *
   * Validator is active if it has the required data to perform validation.
   *
   * @return bool
   *    TRUE if restricted words are set.
   */
  public function isActive() {
    return !empty($this->restrictedWords);
  }

  /**
   * Set / Replace a list of restricted words.
   *
   * @param array $restrictedWords
   */
  public function setRestrictedWords($restrictedWords) {
    $newWords = array();
    if (!empty($restrictedWords) && is_array($restrictedWords)) {
      foreach ($restrictedWords as $word) {
        if (strlen($word) > 0) {
          $newWords[] = $word;
        }
      }

      $this->restrictedWords = $newWords;
    }
  }

}
