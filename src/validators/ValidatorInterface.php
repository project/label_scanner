<?php

/**
 * @file
 * Validator Interface.
 */

/**
 * Class implementing ValidatorInterface can be used to validate text.
 */
interface ValidatorInterface {

  /**
   * Check if $text is valid.
   *
   * @param string $text
   *    Sting to validate.
   *
   * @return bool
   *    True when $text is valid, false otherwise.
   */
  public function isValid($text);

  /**
   * Check if validator is active.
   *
   * @return bool
   *    True if validator has all required data to perform validation.
   */
  public function isActive();

}
