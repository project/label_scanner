<?php

/**
 * @file
 * HTML mail system.
 */

/**
 * Label Scanner mail system class allowing to send HTML emails.
 */
class HTMLMailSystem extends DefaultMailSystem {

  /**
   * @see DefaultMailSystem::format()
   */
  public function format(array $message) {
    if (stripos($message['headers']['Content-Type'], 'html') !== FALSE) {
      $message['body'] = implode("\n\n", $message['body']);
      return $message;
    }
    else {
      return parent::format($message);
    }
  }

}
