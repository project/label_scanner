<?php

/**
 * @file
 * Email notification.
 */

/**
 * Allows to build and send a notification by email.
 */
class EmailNotification implements \SplObserver {

  /**
   * List of emails to send notifications to.
   *
   * @var array
   */
  private $emails = array();

  /**
   * Email message to send.
   *
   * @var string
   */
  private $message;

  /**
   * EmailNotification constructor.
   */
  public function __construct($emails = array()) {
    $this->setEmails($emails);
  }

  /**
   * Implements SplObserver::update().
   *
   * @param \SplSubject $subject
   *   LabelScanner object to which this notification was attached.
   */
  public function update(SplSubject $subject) {
    // Build message.
    $lang = language_default();

    $params = array();
    $params['subject'] = t('Label Scanner Notification: invalid label(s)', array(), array('langcode' => $lang->language));
    $params['body'][] = "<html><head>
    <style type=\"text/css\">
    table {
        border-collapse: collapse;
    }
    th, td {
      border: 1px solid black;
      padding: 5px;
    }
    </style></head><body>";

    $variables['@site_url'] = $GLOBALS['base_url'];
    $variables['@site_name'] = variable_get('site_name', 'Drupal');
    $params['body'][] = "<p>" . t("This is to notify you that the following label(s) on site <a href='@site_url'>@site_name</a>, contain(s) restricted word(s) .", $variables, array('langcode' => $lang->language)) . "</p>";
    $params['body'][] = "<table>";
    $params['body'][] = "<tr>";
    $params['body'][] = "<th>" . t('Label Name') . "</th>";
    $params['body'][] = "<th>" . t('Node / Entity type') . "</th>";
    $params['body'][] = "<th>" . t('Node / Entity title') . "</th>";
    $params['body'][] = "<th>" . t('Status') . "</th>";
    $params['body'][] = "<th>" . t('Author') . "</th>";
    $params['body'][] = "</tr>";

    /** @var ComponentInterface $component */
    foreach ($subject as $component) {
      /** @var EntityMetadataWrapper $entity */
      $entity = $component->getEntity();

      $entity_uri = entity_uri($entity->type(), $entity->value());
      $entity_uri['options']['absolute'] = TRUE;

      $params['body'][] = "<tr>";
      $params['body'][] = "<td>" . check_plain($component->getLabel()) . "</td>";
      $params['body'][] = "<td>" . $entity->type() . " (" . $entity->getBundle() . ")</td>";
      $params['body'][] = "<td><a href='" . url($entity_uri['path'], $entity_uri['options']) . "'>" . $entity->title->value() . "</a></td>";
      $params['body'][] = "<td>" . ($entity->status->value() ? t('published') : t('not published')). "</td>";
      $params['body'][] = "<td>" . $entity->author->name->value() . " (" . $entity->author->mail->value() . ")</td>";
      $params['body'][] = "</tr>";
    }

    $variables['@label_scanner_admin'] = $GLOBALS['base_url'] . '/' . ADMIN_CONFIG_LABEL_SCANNER;
    $params['body'][] = "<p>" . t("To view label scanner's administration options, go to <a href='@label_scanner_admin'>the Label Scanner Administration interface.</a>.", $variables, array('langcode' => $lang->language)) . "</p>";
    $params['body'][] = "</body></html>";

    // Send notification email.
    drupal_mail('label_scanner', 'invalid_label', $this->getEmails(TRUE), $lang, $params);
  }

  /**
   * Validate email address.
   *
   * @param string $email
   *   Email address to validate.
   *
   * @return bool
   *    TRUE if the address is in a valid format.
   */
  public function isValidEmail($email) {
    return valid_email_address($email);
  }

  /**
   * Set a list of email addresses to send notifications to.
   *
   * @param array $emails
   *   List of email addresses.
   */
  public function setEmails($emails) {
    if (!empty($emails)) {
      foreach ($emails as $one_email) {
        $this->addEmail($one_email);
      }
    }
    else {
      throw new UnexpectedValueException(t('Empty list of email addresses provided. Email notifications will fail.'));
    }
  }

  /**
   * Get list of emails.
   *
   * @param bool $asString
   *   If set to TRUE, list of emails will be returned as a
   *   comma separated string.
   *
   * @return array
   *    List of emails to send notifications to.
   */
  public function getEmails($asString = FALSE) {

    if ($asString && $this->emails) {
      return implode(",", $this->emails);
    }
    return $this->emails;
  }

  /**
   * Add email address.
   *
   * @param string $email
   *    Email address.
   */
  public function addEmail($email) {
    if ($this->isValidEmail($email)) {
      $this->emails[] = $email;
    }
    else {
      throw new UnexpectedValueException(t('Invalid email address: @email',
        array('@email' => $email)));
    }
  }

}
