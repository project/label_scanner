<?php

/**
 * @file
 * Drush notification.
 */

/**
 * Drush report.
 */
class DrushNotification implements \SplObserver {

  /**
   * Formats and prints list of invalid labels on the command line.
   *
   * @see \SplObserver::update()
   */
  public function update(SplSubject $subject) {
    $variables['@site_url'] = $GLOBALS['base_url'];
    $variables['@site_name'] = variable_get('site_name', 'Drupal');
    $scannername = $subject->getScannername();
    $title = strtoupper($scannername)." SCANNER REPORT";
    $message = PHP_EOL . $title . PHP_EOL;
    $message .= str_repeat("-", strlen($title)) . PHP_EOL . PHP_EOL;
    $message .= dt("The following label(s) contain(s) restricted word(s) " . PHP_EOL);
    $message .= dt("Site name: @site_name (@site_url) " . PHP_EOL, $variables);

    $rows[] = array($scannername, 'Entity type', 'Entity title', 'URL', 'Status', 'Author');
    /** @var Interface $component */
   foreach ($subject as $component) {
      /** @var EntityMetadataWrapper $entity */
      $entity = $component->getEntity();
      $entity_uri = entity_uri($entity->type(), $entity->value());
      $entity_uri['options']['absolute'] = TRUE;

      $rows[] = array(
        $component->getLabel(),
        $entity->type() . " (" . $entity->getBundle() . ")",
        $entity->title->value(),
        url($entity_uri['path'], $entity_uri['options']),
        $entity->status->value() ? dt('published') : dt('not published'),
        $entity->author->name->value() . " (" . $entity->author->mail->value() . ")",
      );
    }

    // Do nothing if the report is empty.
    if (count($rows) > 1) {
      drush_print($message);
      drush_print_table($rows, TRUE);
    }
  }

}
