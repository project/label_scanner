<?php

/**
 * @file
 * Drupal message notification.
 */

/**
 * Notifies a user about a restricted label by displaying a message in the UI.
 */
class MessageNotification implements \SplObserver {

  /**
   * Implements SplObserver::update().
   *
   * @param \SplSubject $subject
   *   LabelScanner object to which this notification was attached.
   */
  public function update(SplSubject $subject) {
    /** @var ComponentInterface $component */
    foreach ($subject as $component) {
      drupal_set_message(t("Warning: @type with label <b>'%label'</b> contains a restricted word. You should not be using restricted words in your labels.", array(
        '@type' => ucwords($component->getComponentType()),
        '%label' => $component->getLabel(),
      )));
    }
  }

}
