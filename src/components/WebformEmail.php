<?php

/**
 * @file
 * Contains WebformEmail class.
 */

/**
 * Wrapper for Webform email.
 */
class WebformEmail implements WebformEmailInterface {

  /**
   * Webform email data.
   *
   * Data as stored in the DB or as returned by
   * the webform email edit form.
   *
   * @var array
   */
  private $componentData;

  /**
   * Entity to which webform  belongs.
   *
   * Example: webform  belongs to a node entity.
   *
   * @var EntityMetadataWrapper
   */
  private $entity;


  /**
   * Webform email template token.
   *
   * Token used in Webform email template data.
   *
   * @var string
   */
  private $reason;

  /**
   * WebformEmail constructor.
   *
   * @var array $componentData
   * Webform email data as loaded in the webform
   * node or submitted from the webform email edit form.
   * @var EntityMetadataWrapper $entity
   *  Webform entity to which the email belongs.
   */
  public function __construct($componentData, EntityMetadataWrapper $entity) {
    $this->componentData = $componentData;
    $this->entity = $entity;
  }

  /**
   * Checks if webform_emails has email address.
   *
   * @return bool
   *   Retruns email address.
   */
  public function hasEmail() {
    return $this->componentData['email'];
  }

  /**
   * Checks if the component has  email.
   */
  public function getEmail() {
    if ($this->hasEmail()) {
      return $this->componentData['email'];
    }
    return;
  }

  /**
   * Returns data of email template.
   */
  public function getTemplate() {
    return $this->componentData['template'];
  }

  /**
   * Returns data of  excluded_components.
   */
  public function getExcludedcomponents() {
    return $this->componentData['excluded_components'];
  }

  /**
   * Returns eid.
   */
  public function getEid() {
    return $this->componentData['eid'];
  }

  /**
   * Returns checkbox value of "Include files as attachments".
   */
  public function getAttachments() {
    return $this->componentData['attachments'];
  }
  /**
   * Get domain and reason.
   *
   * @return string
   *   Returns information to notify
   */
  public function getLabel() {
    if ($this->hasCid()) {
      return $this->componentData['eid'] . " " . $this->componentData['email'] . "(" . $this->getComponentName($this->componentData['email']) . ")" . " " . $this->reason;
    }
    return $this->componentData['eid'] . " " . $this->componentData['email'] . " " . $this->reason;
  }

  /**
   * Checks if webform_emails has email address or component id.
   *
   * @return bool
   *   Return bool.
   */
  public function hasCid() {
    $component = $this->getComponent();
    return array_key_exists($this->componentData['email'], $component);
  }

  /**
   * Retrieves webform components.
   *
   * @return array
   *   Returns array of webform components.
   */
  public function getComponent() {
    return $this->getEntity()->value()->webform['components'];
  }

  /**
   * Retrieves webform components.
   *
   * @var cid
   * @return array
   *   Return component name.
   */
  public function getComponentName($cid) {
    $component = $this->getComponent();
    return $component[$cid]['name'];
  }

  /**
   * Setter for validation reason.
   */
  public function setReason($reason) {
    return $this->reason = $reason;
  }

  /**
   * Getter function for component data.
   */
  public function __get($name) {
    if (!empty($this->componentData[$name])) {
      return $this->componentData[$name];
    }

    return NULL;
  }

  /**
   * Gets component's node ID.
   */
  public function getNodeId() {
    if (!empty($this->componentData['nid'])) {
      return $this->componentData['nid'];
    }
  }

  /**
   * Get entity to which component belongs.
   *
   * @return \EntityMetadataWrapper
   *   Entity to which the component belongs.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Print component data.
   */
  public function __toString() {
    $data = array();
    $data['Node id'] = $this->entity->getIdentifier();
    $data['Email'] = $this->getEmail();
    return (string) print_r($data, 1);
  }

}
