<?php

/**
 * @file
 * Contains WebformComponent class.
 */

/**
 * Wrapper for Webform component.
 */
class WebformComponent implements ComponentInterface {

  /**
   * Webform component data.
   *
   * Data as stored in the DB or as returned by
   * the webform component edit form.
   *
   * @var array
   */
  private $componentData;

  /**
   * Entity to which component belongs.
   *
   * Example: webform component belongs to a node entity.
   *
   * @var EntityMetadataWrapper
   */
  private $entity;

  /**
   * WebformComponent constructor.
   *
   * @var array $componentData
   * Webform component data as loaded in the webform
   * node or submitted from the webform component edit form.
   * @var EntityMetadataWrapper $entity
   *  Webform entity to which the component belongs.
   */
  public function __construct($componentData, EntityMetadataWrapper $entity) {
    $this->componentData = $componentData;
    $this->entity = $entity;
  }

  /**
   * Checks if the component has a label.
   *
   * @see \webform_component_feature()
   */
  public function hasLabel() {
    if (webform_component_feature($this->getComponentType(), 'title') && !empty($this->componentData['name'])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Gets component's label, if one exists / is set.
   *
   * @return string
   *    label
   */
  public function getLabel() {
    if ($this->hasLabel()) {
      return $this->componentData['name'];
    }
  }

  /**
   * Getter function for component data.
   */
  public function __get($name) {
    if (!empty($this->componentData[$name])) {
      return $this->componentData[$name];
    }

    return NULL;
  }

  /**
   * Gets component's node ID.
   */
  public function getNodeId() {
    if (!empty($this->componentData['nid'])) {
      return $this->componentData['nid'];
    }
  }

  /**
   * Gets component's ID.
   */
  public function getComponentId() {
    if (!empty($this->componentData['cid'])) {
      return $this->componentData['cid'];
    }
  }

  /**
   * Get component type.
   */
  public function getComponentType() {
    return $this->type;
  }

  /**
   * Get entity to which component belongs.
   *
   * @return \EntityMetadataWrapper
   *   Entity to which the component belongs.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Print component data.
   */
  public function __toString() {
    $data = array();
    $data['Component id'] = $this->getComponentId();
    $data['Node id'] = $this->entity->getIdentifier();
    $data['Component Label'] = $this->getLabel();

    return (string) print_r($data, 1);
  }

}
