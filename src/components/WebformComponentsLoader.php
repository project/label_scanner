<?php

/**
 * @file
 * Webform component loader.
 */

/**
 * Class WebformComponentsLoader.
 *
 * Loads webform nodes and their components.
 * Lazy loads components in batches.
 */
class WebformComponentsLoader {

  /**
   * Node id of last component retrieved.
   *
   * @var int
   */
  private $lastNid = 0;

  /**
   * Batch size.
   *
   * @var int
   */
  private $batchSize;

  /**
   * Configuration options.
   *
   * @var LabelScannerDataMapper
   */
  private $options;

  /**
   * WebformComponentsLoader constructor.
   *
   * @param LabelScannerDataMapper $options
   * @param int $batchSize
   *   Optional batch size.
   */
  public function __construct(LabelScannerDataMapper $options, $batchSize = 20) {
    $this->options = $options;
    $this->setBatchSize($batchSize);
  }

  /**
   * Set batch size.
   *
   * @param int $batchSize
   *   Number of components to load in one batch.
   */
  public function setBatchSize($batchSize) {
    if (is_int($batchSize) && $batchSize > 0) {
      $this->batchSize = $batchSize;
    }
  }

  /**
   * Loads and returns a batch of webform components.
   *
   * @return WebformComponent[]
   *   A batch of webform components.
   */
  public function loadNextBatch() {
    $components = array();

    $query = db_select('node', 'n');
    $query
      ->condition('type', 'webform');

    if ($this->options->scanPublishedOnly()) {
      $query->condition('status', 1);
    }

    $query->condition('nid', $this->lastNid, '>')
      ->fields('n', array('nid', 'type'))
      ->orderBy('nid')
      ->range(NULL, $this->batchSize);

    $result = $query->execute();

    // Loop through webform nodes.
    foreach ($result as $node) {
      $entity = entity_metadata_wrapper('node', $node->nid);

      // Loop through each component in a webform node.
      if (!empty($entity->value()->webform['components']) && is_array($entity->value()->webform['components'])) {
        $componentsData = $entity->value()->webform['components'];
        foreach ($componentsData as $oneComponent) {
          $components[] = new WebformComponent($oneComponent, $entity);
        }
      }

      $this->lastNid = $node->nid;
    }

    return $components;
  }

}
