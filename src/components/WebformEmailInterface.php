<?php

/**
 * @file
 * WebformEmail Interface.
 */

/**
 * Implement WebformEmailInterface to be able scan webforms' email.
 */
interface WebformEmailInterface {

  /**
   * Check if webform has a email.
   */
  public function hasEmail();

  /**
   * Returns webform's email.
   */
  public function getEmail();

  /**
   * Entity to which component belongs.
   *
   * @return \EntityMetadataWrapper Parent entity.
   */
  public function getEntity();

}
