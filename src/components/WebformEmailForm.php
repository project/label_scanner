<?php

/**
 * @file
 * Contains WebformEmailForm class.
 */

/**
 * Wrapper for Webform email form.
 */
class WebformEmailForm implements WebformEmailInterface {

  /**
   * Webform email form data.
   *
   * Data as returned by the webform email edit form.
   *
   * @var array
   */
  private $componentData;

  /**
   * Entity to which webform belongs.
   *
   * Example: webform belongs to a node entity.
   *
   * @var EntityMetadataWrapper
   */
  private $entity;


  /**
   * Webform email template token.
   *
   * Token used in Webform email template data.
   *
   * @var string
   */
  private $reason;

  /**
   * WebformEmailForm constructor.
   *
   * @var array $componentData
   * Webform email data as submitted from the webform email edit form.
   * @var EntityMetadataWrapper $entity
   *  Webform entity to which the email belongs.
   */
  public function __construct($componentData, EntityMetadataWrapper $entity) {
    $this->componentData = $componentData;
    $this->entity = $entity;
  }

  /**
   * Checks if webform_emails has email address.
   *
   * @return bool
   *   Retruns TRUE if form has email, FALSE otherwise.
   */
  public function hasEmail() {
    if (
      !empty($this->componentData['email_option'])
      && (
        (
          $this->componentData['email_option'] == 'custom'
          && !empty($this->componentData['email_custom'])
        )
        || (
          $this->componentData['email_option'] == 'component'
          && !empty($this->componentData['email_component'])
        )
      )
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Return the email value from the form, either custom or component.
   */
  public function getEmail() {
    if ($this->hasEmail()) {
      if ($this->componentData['email_option'] == 'custom') {
        return $this->componentData['email_custom'];
      }
      if ($this->componentData['email_option'] == 'component') {
        $cid = $this->componentData['email_component'];
        if ($this->getComponentType($cid) !== 'email') {
          if (!empty($this->componentData['email_mapping'][$cid])) {
            $emails = array_filter($this->componentData['email_mapping'][$cid]);
            return implode(',', $emails);
          }
        }
        return $cid;
      }
    }
    return;
  }

  /**
   * Returns data of email template.
   */
  public function getTemplate() {
    if (isset($this->componentData['template'])) {
      return $this->componentData['template'];
    }
    return '';
  }

  /**
   * Setter for validation reason.
   */
  public function setReason($reason) {
    return $this->reason = $reason;
  }

  /**
   * Get entity to which component belongs.
   *
   * @return \EntityMetadataWrapper
   *   Entity to which the component belongs.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Retrieves a specified value submitted in the form.
   */
  public function getFormValue($name) {
    if (!empty($this->componentData[$name])) {
      return $this->componentData[$name];
    }
    return NULL;
  }

  /**
   * Retrieves a webform component's type.
   *
   * @var cid
   * @return string
   *   Return component type.
   */
  public function getComponentType($cid) {
    $components = $this->getEntity()->value()->webform['components'];
    if (isset($components[$cid]['type'])) {
      return $components[$cid]['type'];
    }
    return '';
  }

}
