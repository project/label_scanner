<?php

/**
 * @file
 * Component Interface.
 */

/**
 * Implement ComponentInterface to be able scan component's labels.
 */
interface ComponentInterface {

  /**
   * Check if component has a label.
   */
  public function hasLabel();

  /**
   * Returns component's label.
   */
  public function getLabel();

  /**
   * Entity to which component belongs.
   *
   * @return \EntityMetadataWrapper Parent entity.
   */
  public function getEntity();

}
