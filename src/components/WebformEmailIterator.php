<?php

/**
 * @file
 * webform_email Iterator: a list of webform_email.
 */

/**
 * Class WebformEmailIterator.
 *
 * Iterates over a list of webform_emails.
 * Given a loader (@see \WebformEmailIterator::setLoader()), lazy loads components
 * in batches.
 */
class WebformEmailIterator implements Iterator, Countable {

  /**
   * List of webform_emails.
   *
   * @var arrayComponentInterface[]
   */
  private $components = array();

  /**
   * Current position in list.
   *
   * @var intCurrentpositioninthecomponentsarray
   */
  private $position = 0;

  /**
   * Loader to use to load a batch of webform_emails.
   *
   * @var WebformComponentsLoader
   */
  private $loader;

  /**
   * WebformEmailIterator constructor.
   *
   * Optionally initialized a list of webform_emails.
   *
   * @param WebformEmailInterface[] $components
   *   List of components.
   */
  public function __construct($components = array()) {
    $this->components = $components;
  }

  /**
   * Add a loader instance if data needs to be loaded.
   *
   * @param \WebformComponentsLoader $loader Instance of loader object.
   */
  public function setLoader(\WebformEmailsLoader $loader) {
    $this->loader = $loader;
  }

  /**
   * @see \Iterator::current()
   */
  public function current() {
    return $this->components[$this->position];
  }

  /**
   * @see \Iterator::next()
   */
  public function next() {
    ++$this->position;
  }

  /**
   * @see \Iterator::key()
   */
  public function key() {
    return $this->position;
  }

  /**
   * Checks if a webform_emails exists at current position.
   *
   * If a loader instance was provided, loads webform_emails when there is no
   * webform_emails at current position.
   *
   * @see \Iterator::valid()
   */
  public function valid() {
    if (!empty($this->components[$this->position])) {
      return TRUE;
    }

    // Load next batch of webform_emails if a loader was provided.
    if (isset($this->loader)) {
      $components = $this->loader->loadNextBatch();
      if (!empty($components)) {
        // Add another batch of components.
        $this->components = array_merge($this->components, $components);
        return $this->valid();
      }
    }

    return FALSE;
  }

  /**
   * @see \Iterator::rewind()
   */
  public function rewind() {
    $this->position = 0;
  }

  /**
   * @return int
   *   Number of webform_emails.
   *
   * @see \Countable::count()
   */
  public function count() {
    // If we have a loader, we need to iterate over webform_emails to load them.
    if (isset($this->loader)) {
      foreach ($this as $component) {
      }
    }
    return count($this->components);
  }

  /**
   * Add a webformEmail to the list.
   *
   * @param \WebformEmailInterface $component
   *   Component to append to the list.
   */
  public function append(WebformEmailInterface $component) {
    $this->components[] = $component;
  }


}
