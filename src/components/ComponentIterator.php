<?php

/**
 * @file
 * Component Iterator: a list of components.
 */

/**
 * Class ComponentIterator.
 *
 * Iterates over a list of components.
 * Given a loader (@see \ComponentIterator::setLoader()), lazy loads components
 * in batches.
 */
class ComponentIterator implements Iterator, Countable {

  /**
   * List of components.
   *
   * @var arrayComponentInterface[]
   */
  private $components = array();

  /**
   * Current position in list.
   *
   * @var intCurrentpositioninthecomponentsarray
   */
  private $position = 0;

  /**
   * Loader to use to load a batch of components.
   *
   * @var WebformComponentsLoader
   */
  private $loader;

  /**
   * ComponentIterator constructor.
   *
   * Optionally initialized a list of components.
   *
   * @param ComponentInterface[] $components
   *   List if components.
   */
  public function __construct($components = array()) {
    $this->components = $components;
  }

  /**
   * Add a loader instance if data needs to be loaded.
   *
   * @param \WebformComponentsLoader $loader Instance of loader object.
   */
  public function setLoader(\WebformComponentsLoader $loader) {
    $this->loader = $loader;
  }

  /**
   * @see \Iterator::current()
   */
  public function current() {
    return $this->components[$this->position];
  }

  /**
   * @see \Iterator::next()
   */
  public function next() {
    ++$this->position;
  }

  /**
   * @see \Iterator::key()
   */
  public function key() {
    return $this->position;
  }

  /**
   * Checks if a component exists at current position.
   *
   * If a loader instance was provided, loads components when there is no
   * component at current position.
   *
   * @see \Iterator::valid()
   */
  public function valid() {
    if (!empty($this->components[$this->position])) {
      return TRUE;
    }

    // Load next batch of components if a loader was provided.
    if (isset($this->loader)) {
      $components = $this->loader->loadNextBatch();
      if (!empty($components)) {
        // Add another batch of components.
        $this->components = array_merge($this->components, $components);
        return $this->valid();
      }
    }

    return FALSE;
  }

  /**
   * @see \Iterator::rewind()
   */
  public function rewind() {
    $this->position = 0;
  }

  /**
   * @return int
   *   Number of components.
   *
   * @see \Countable::count()
   */
  public function count() {
    // If we have a loader, we need to iterate over components to load them.
    if (isset($this->loader)) {
      foreach ($this as $component) {
      }
    }
    return count($this->components);
  }

  /**
   * Add a component to the list.
   *
   * @param \ComponentInterface $component
   *   Component to append to the list.
   */
  public function append(ComponentInterface $component) {
    $this->components[] = $component;
  }

}
